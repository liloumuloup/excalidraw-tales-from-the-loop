# Excalidraw Tales From the Loop

## Qu'est-ce que c'est ?
Ceci est le repo contenant le fichier Excalidraw pour la campagne de Khildar sur Tales from the Loop. Il est à importer sur le site  [Excalidraw](https://excalidraw.com/) pour être utilisable.

## Méthologie d'importation
- [Télécharger le fichier .excalidraw](https://framagit.org/liloumuloup/excalidraw-tales-from-the-loop/-/blob/main/Tales%20from%20the%20loop%20liens.excalidraw)
- Ouvrir Excalidraw : [https://excalidraw.com/](https://excalidraw.com/)
- Importer le fichier :

![alt text](doc/import.png "Import Excalidraw")

## Méthodologie d'exportation
- Exporter le fichier : 

![alt text](doc/export.png "Export Excalidraw")
- Envoyer le fichier à Lilou pour qu'elle le mette à jour sur ce dépôt parce qu'elle a la flemme d'expliquer Git
